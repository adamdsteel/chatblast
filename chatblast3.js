"use strict";

var geolib = require('geolib');
var chatlocation = require('./chatlocation.js');

const roomPrefix = "chatroom";

const DEFAULT_MAXIMUM_DISTANCE = 20;

const expressInstance = require('express');
const expressApp = expressInstance();

const http = require('http').Server(expressApp);
const io = require('socket.io')(http);

expressApp.get('/', (req, res) => res.send('Hello World!'));
expressApp.use(expressInstance.static('files'));

var userCount = 0;

io.on('connection', onConnect);

var waitingUsers = [];
var rooms = [];

    
http.listen(3000, () => console.log('Example app listening'))

//When a user joins:
function onConnect(connectionSocket)
{
    
    userCount += 1;
    console.log("User count: " +  userCount);
    
    connectionSocket.on("disconnect", function(){
                                                onDisconnect(connectionSocket);
                                                });
    
    
    connectionSocket.on("userready", 
                        function(incMessage){
                                            onReady(incMessage, connectionSocket);
                                            });
    
    connectionSocket.on("standardmessage", 
                        function(incMessage){
                                            onStandardMessage(incMessage, connectionSocket);
                                            });
    
    connectionSocket.emit("connectionmade");
    //globalSocket = connectionSocket;
    //Setting up what happens when we disconnect:
    
    console.log("User has connected");
    //socket.$broadcast("Hey");
    
};

//When the user is ready to join a chat:
function onReady(message, givenSocket)
{
    console.log("user ready - data: " + message);
    
    
    console.log("Message: %j", message);
    
    var newUser;

    try
    {
        
    
        newUser = {socket: givenSocket,
                   gender: message.userGender,
                   
                   desiredGenders: {
                       males: message.desiredGenders.male,
                       females: message.desiredGenders.female, 
                       other: message.desiredGenders.other,
                   },
                  
                    location: message.location,
                    maximumDist: DEFAULT_MAXIMUM_DISTANCE
                       
                  };
        
        console.log(" ");
        
    }
    catch(err)
    {
        console.log(err + " - Error! Aborting ready user.");
        givenSocket.emit("messagetoclient", "Error processing user data. Reload and try again.");
        givenSocket.disconnect();
    }

    var matched = false;
    
    //Iterate thru waitingUsers[] until we reach the end or match the user.
    for(var i = 0; i < waitingUsers.length && !matched; i++)
    {
        
        //This is the worst if statement in the universe
        if( usersAreMatched(newUser, waitingUsers[i])
          )
            {
                console.log("Found two matching users!")
                
                
                
                var tempWaitingUser = waitingUsers[i];
                
                waitingUsers = waitingUsers.splice(i, 1); //Remove the waiting user from the array, very inefficiently.
                matched = true;
                
                
                //Create room
                var newRoom = {user1: newUser,
                               user2: tempWaitingUser,
                               uniqueID: roomPrefix + getUniqueID(),
                              };

                rooms.push(newRoom);

                newRoom.user1.socket.join(newRoom.uniqueID);
                newRoom.user1.socket.canonicalRoom = newRoom.uniqueID;

                newRoom.user2.socket.join(newRoom.uniqueID)
                newRoom.user2.socket.canonicalRoom = newRoom.uniqueID;


                //Tell the clients their room number:
                console.log("Emitting message to room " + newRoom.uniqueID);
                io.to(newRoom.uniqueID).emit("roomnumber", newRoom.uniqueID);

                rooms.push(newRoom);
                
                
                //Test:
                io.to(newRoom.uniqueID).emit("messagetoclient", "Connnected to a person approximately " + Math.round(chatlocation.usersDistance(newRoom.user1, newRoom.user2)) + "km away" );
                //-----
            }
        
        
    }
    
    
    if(!matched)
    {
        waitingUsers.push(newUser);
    }
}

function usersAreMatched(user1, user2)
{   
    var distanceBetween = chatlocation.usersDistance(user1, user2);
    
    if(  distanceBetween < user1.maximumDist
      && distanceBetween < user2.maximumDist)
    {
        return true;
    }
    else
    {
        return false;
    }
}



function usersAreMatchedOnGender(user1, user2) //Gender version
{ 
    
    var preferenceMatched = false;
    //Does user2 suit user1's preference?
    if(       (user1.desiredGenders.males && user2.gender == "male"
            || user1.desiredGenders.females && user2.gender == "female"
            || user1.desiredGenders.other && user2.gender == "other")
         &&
              (user1.gender == "male" && user2.desiredGenders.males
            || user1.gender == "female" && user2.desiredGenders.females
            || user1.gender == "other" && user2.desiredGenders.other ) )
        {
            if(true) //Insert location matching code here
                {
                    return true
                }
        }
    else
        {return false}
    
    //Are they close?
}

/*
function usersAreMatched(user1, user2)
{
        (user1.desiredGenders.males && user2.gender == "male"
            || user1.desiredGenders.females && user2.gender == "female"
            || user1.desiredGenders.other && user2.gender == "other" )
            &&
            ( user1.gender == "male" && user2.desiredGenders.males
            || user1.gender == "female" && user2.desiredGenders.females
            || user1.gender == "other" && user2.desiredGenders.other )
}
*/

/*
function attemptConnectUsers()
{
    console.log("attemptConnectUsers");
    
    if(connectUsers() == false)
    {
        var uInterval = setInterval(
            function(){
                        if(connectUsers() == true)
                            {
                                clearInterval(uInterval);
                            }
                        }, 100);
    }
}

function connectUsers()
{
    console.log("connectUsers");
    
    if(connectingUsers == true)
    {
        return false;
    }
    else
    {
        connectingUsers = true; //Lock the users.        
    
        var tempUser1 = waitingUsers.pop();
        var tempUser2 = waitingUsers.pop();
        
        //Create room
        var newRoom = {user1: tempUser1,
                       user2: tempUser2,
                       uniqueID: roomPrefix + getUniqueID(),
                      };
        
        rooms.push(newRoom);
        
        
        
        newRoom.user1.socket.join(newRoom.uniqueID);
        newRoom.user1.socket.canonicalRoom = newRoom.uniqueID;
        
        newRoom.user2.socket.join(newRoom.uniqueID)
        newRoom.user2.socket.canonicalRoom = newRoom.uniqueID;
 

        //Tell the clients their room number:
        console.log("Emitting message to room " + newRoom.uniqueID);
        io.to(newRoom.uniqueID).emit("roomnumber", newRoom.uniqueID);

        rooms.push(newRoom);
        
        console.log("Users waiting: " + waitingUsers.length);

        connectingUsers = false; //Unlock the users.
        
        return true;
    }
}
*/

function joinCallback(joinError)
{
    console.log("Join errors: " + joinError);
}

    
function getUniqueID()
{

    var newUniqueID;
    var unique = false;
    
    while(unique == false)
    {//This could be improved in efficiency:
        
        newUniqueID = Math.round(Math.random() * 100000);
        console.log(newUniqueID);
        
        unique = true; //We don't actually know this yet.
        
        for(var i = 0; i < rooms.length; i++)
        {
            if(rooms[i].uniqueID == newUniqueID)
                {
                    unique = false;
                    break;
                }
        }
    }
    
    return newUniqueID;
}

function createRoom()
{
    rooms.push
}

function onDisconnect(socket)
{
    userCount -= 1;
    console.log("User count: " +  userCount);
    
    //Identify the room and disconnect the other user
    io.to(socket.canonicalRoom).emit("roomclosed");
    
    socket.disconnect();
}

function onStandardMessage(message, socket)
{
    var messageRoom = socket.canonicalRoom;
    
    console.log("User sent: \'" + message + "\', from room: " + messageRoom);
    
    //outgoingSocket = //FIND THE SOCKET FOR THE CHAT PARTNER

    io.to(messageRoom).emit("messagetoclient", message);
}


//Objects:

class Room
{
    
}

class User
{
    
}

