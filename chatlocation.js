"use strict";

var geolib = require('geolib');

exports.usersDistance = function(user1, user2)
{

    try
    {
        return(geolib.getDistance(
                user1.location,
                user2.location))/1000; // /1000 is conversion from m to km
    }
    catch(err)
    {
        console.log("Failure getting distance between users: " + err.message);
    }
    
}