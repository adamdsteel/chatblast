/*global $ */

// Copyright Adam Steel 2018
// 
// Name: chatClient3.js
// Decription: This file contains the entry point of JavaSript on the chat client, 
// as well as the chat code, and most of the UI. A small amount UI stuff was spun out into chatui.js.

const IO_SERVER = "http://192.168.0.3:3000";

var globalObject = { testProperty: 'blah',
                     userPosition: {}
                   };

$(function(){

    
    $(document).ready(pageLoaded);
    console.log("This");
    $(document).keydown(chatboxSubmit);
    $("#userDataButton").click(userFormSubmit);
    
    $("#chatarea").hide();
    
    $("#userDataButton").prop('disabled', true);
    $("#userform").click(checkButton);
    
    //$("#chatarea").focusin( function(){$("#chatarea").val("")}  );
    
    //GeoLocation stuff:
    var chatPositionOptions = {enableHighAccuracy:true};
    if(navigator.geolocation)
    {
        navigator.geolocation.getCurrentPosition(positionSuccess, positionErrorHandler, chatPositionOptions);
    }
});

var socketInstance;
var roomID;



function onConnection(socket)
{  
    console.log("Connected to server"); 

}

function logMessage(text)
{
    $("#textscrollarea").append("<p>" + text + "</p>");
}

function checkButton()
{
    

    //If a radio button is selected,
    //and at least one checkbox is selected:
    if($("input[name='usergender']:checked").val() !== undefined
       && $("input[name='desiredgenders']:checked").length > 0    ) 
        {
        $("#userDataButton").prop('disabled', false);
        }
    else
        {
            $("#userDataButton").prop('disabled', true);
        }
}

function pageLoaded()
{
    console.log("Page loaded - new version");
    
    socketInstance = io.connect(IO_SERVER);
    
    socketInstance.on("connectionmade", onConnection);
    socketInstance.on("messagetoclient", messageReceived);
    socketInstance.on("roomnumber", setRoom);
    socketInstance.on("roomclosed", removedFromRoom);
}

function userFormSubmit(event)
{
    event.preventDefault();
    
    console.log("positon stringified:");
    console.log(JSON.stringify(globalObject.userPosition));
        
    var userData = {userGender: $("input[name='usergender']:checked").val(),
                    desiredGenders: { male: $("#malebox").is(':checked'),
                                      female: $("#femalebox").is(':checked'),
                                      other: $("#otherbox").is(':checked') },
                    
                    location: {latitude: 0, //globalObject.userPosition.coords.latitude,
                               longitude: 0}, //globalObject.userPosition.coords.longitude},
                   }
    
    
    //var userData = $("#userform").serializeArray();
    
    console.log("userData: ");
    console.log(userData);
    socketInstance.emit("userready", userData);
    
    $('#formouter').hide();
    
}
    

function chatboxSubmit(event)
{
    if(event.which == 13 ) //13 is the enter key. TODO: Check if this is the 'correct' way to do this (accessibility, mobile, etc)
    {
        event.preventDefault(); 

        console.log("Submitting chatbox...: " + $('#chatarea').val());
        socketInstance.emit("standardmessage", $('#chatarea').val());
        $('#chatarea').val(""); //Clear the textbox
    }
        //event.preventDefault();
}

function messageReceived(message)
{
    $("#textscrollarea").append("<p>" + message + "</p>");
    console.log("Received: " + message);
}

function setRoom(roomNumber)
{
    roomID = roomNumber;
    console.log("Joined room: " + roomNumber);
    
    $("#chatarea").show();
    $("#formholder").hide();
}

function removedFromRoom(socket)
{
    console.log("Left room"); 
    logMessage("Chat finished");
    $("#restartbuttonarea").show();
    
    socketInstance.disconnect();
}

//To be called when the room closes and the user indicates they're ready to restart
function userReset() 
{
    $('#formouter').show();
}

//Location stuff:
function positionSuccess(userPosition)
{
    globalObject.userPosition = userPosition;

    console.log(userPosition);
    console.log(JSON.stringify(userPosition));
}

function positionErrorHandler(error)
{
    //Just in case we want to do something with this later.
    console.log("Error: " + error);
}
